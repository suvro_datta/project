<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->unsignedBigInteger('customer_id')->nullable();//this should be same as parent primary key
            
            $table->string('order_status')->nullable();
            $table->string('paid')->nullable();
            $table->string('subtotal')->nullable();
            $table->string('tax')->nullable();
            $table->string('shipping')->nullable();
            $table->string('discount')->nullable();

            $table->foreign('customer_id')->references('id')
                  ->on('customers')->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
