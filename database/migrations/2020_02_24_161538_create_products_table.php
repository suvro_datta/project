<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->unsignedBigInteger('category_id')->nullable();//this should be same as parent primary key
            $table->unsignedBigInteger('brand_id')->nullable();//this should be same as parent primary key

            $table->string('barcode')->nullable();
            $table->text('image')->nullable();
            $table->text('images')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('details')->nullable();
            $table->integer('price')->nullable();
            $table->text('description')->nullable();
            $table->boolean('featured')->default(false);
            $table->boolean('status')->default(false);
            $table->unsignedInteger('quantity')->default(1);

            $table->foreign('category_id')->references('id')
                  ->on('categories')->onDelete('cascade');
            $table->foreign('brand_id')->references('id')
                  ->on('brands')->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
