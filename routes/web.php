<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    //'register' => false, // Registration Routes...
]);

Route::get('/', function () {
    return Redirect::to('/admin');
});

Route::any('/admin/{any?}', 'AdminController@index')->where('any','.*')->middleware('auth');

//social settings
Route::get('/social-settings','Socials\SocialController@getSocialSettings');
Route::post('/save-social-settings','Socials\SocialController@saveSocialSettings');
Route::post('/sms/send','Socials\SmsController@sendSms');
Route::post('/email/send','Socials\EmailController@sendEmail');
Route::get('/sms/numbers-with-sms-settings','Socials\SmsController@getNumbersWithSmsSettings');
Route::get('/email/emails-with-sms-settings','Socials\EmailController@getEmailsWithEmailSettings');

//notifications
Route::get('/mark-as-read','Notification\NotificationController@markAsRead');
Route::get('/get-notifications','Notification\NotificationController@getNotification');

//Dashboard 
Route::get('/dashboard-info','Dashboard\DashboardController@dashboardInfo');
Route::get('/timeline-income-expense/{days}','Dashboard\DashboardController@timelineIncomeExpense');

//Cart 
Route::post('/add-to-cart','Pos\CartController@addToCart');
Route::post('/add-to-cart-by-barcode','Pos\CartController@addToCartByBarcode');
Route::get('/cart-products','Pos\CartController@cartProducts');
Route::post('/cart-product','Pos\CartController@cartProduct');
Route::post('/delete-cart','Pos\CartController@deleteCart');
Route::post('/update-cart','Pos\CartController@updateCart');
Route::post('/remove-item-from-cart','Pos\CartController@removeItemFromCart');

//Expense Routes
Route::post('/expense/add-expense','Expense\ExpenseController@addExpense');
Route::get('/expenses','Expense\ExpenseController@index');
Route::get('/expense/edit/{id}','Expense\ExpenseController@edit');
Route::put('/expense/update/{id}','Expense\ExpenseController@update');
Route::delete('/expense/delete/{id}','Expense\ExpenseController@delete');
Route::post('/expenses/deletemultiple','Expense\ExpenseController@deletemultiple');

//pos routes
Route::get('/category-product','Pos\PosController@getCategoryProducts');
Route::post('/find-user','Pos\PosController@findUser');
Route::post('/save-pos','Pos\PosController@savePosData');
Route::get('/all-pos-init-data','Pos\PosController@getInitialPosData');

//Products Routes
Route::get('/products','Products\ProductController@index');
Route::post('/product/create','Products\ProductController@create');
Route::delete('/product/delete/{id}','Products\ProductController@delete');
Route::get('/product/edit/{id}','Products\ProductController@edit');
Route::post('/product/update/{id}','Products\ProductController@update');
Route::post('/products/deletemultiple','Products\ProductController@deletemultiple');
Route::get('/all-categories-and-products','Products\ProductController@allCategoriesAndProducts');

//Categories Routes
Route::get('/categories','Categories\CategoryController@index');
Route::post('/category/create','Categories\CategoryController@create');
Route::delete('/category/delete/{id}','Categories\CategoryController@delete');
Route::get('/category/edit/{id}','Categories\CategoryController@edit');
Route::put('/category/update/{id}','Categories\CategoryController@update');
Route::post('/categories/deletemultiple','Categories\CategoryController@deletemultiple');
//Route::get('/all-categories','Categories\CategoryController@all_category');

//Orders Routes
Route::get('/orders','Order\OrderController@index');

//Customers Routes
Route::get('/customers','Customer\CustomerController@index');

//Brands Routes
Route::get('/brands','Brands\BrandController@index');
Route::post('/brand/create','Brands\BrandController@create');
Route::delete('/brand/delete/{id}','Brands\BrandController@delete');
Route::get('/brand/edit/{id}','Brands\BrandController@edit');
Route::put('/brand/update/{id}','Brands\BrandController@update');
Route::post('/brands/deletemultiple','Brands\BrandController@deletemultiple');
// Route::get('/all-brands','Brands\BrandController@all_brand');

//Permission Routes
Route::get('/permissions','UserManagement\PermissionController@getPermissions');
Route::post('/permission/deletemultiple','UserManagement\PermissionController@deleteMultiple');

//Role Routes
Route::get('/roles','UserManagement\RoleController@index');
Route::post('/role/create','UserManagement\RoleController@create');
Route::post('/role/update','UserManagement\RoleController@update');

//User Routes
Route::get('/users','UserManagement\UserController@index');
Route::post('/user/create','UserManagement\UserController@create');
Route::delete('/user/delete/{id}','UserManagement\UserController@delete');
Route::put('/user/update/{id}','UserManagement\UserController@update');

//General Settings Routes
Route::get('/settings','Settings\SettingsController@index');
Route::post('/update-settings','Settings\SettingsController@update');

//pos settings
Route::post('/update-advance-settings','Settings\AdvanceSettingsController@update');
Route::get('/advance-settings','Settings\AdvanceSettingsController@index');

//Profile Settings Routes
Route::post('/update-password','Settings\ProfileController@updatePassword');
Route::post('/update-avatar','Settings\ProfileController@uploadImage');
Route::post('/update-info','Settings\ProfileController@updateProfileInfo');
Route::get('/get-profile','Settings\ProfileController@getProfileInfo');

//Reports Routes
Route::post('/reports','Reports\ReportController@getReports');
