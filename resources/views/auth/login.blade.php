<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>Login</title>
  <meta name="keywords" content="InventorySoft designed by Bootstrap " />
  <meta name="description" content="InventorySoft - A Most Daynamic Inventory Software">
  <meta name="author" content="InventorySoft">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
  <link rel="icon" type="image/png" href="{{ asset('images/logo/logo.png') }}" />

  <!-- Theme CSS -->
  <link href="{{ asset('admin-asset/admin-login/css/theme.css') }}" rel="stylesheet">

  <!-- Admin Forms CSS -->
  <link href="{{ asset('admin-asset/admin-login/css/admin-forms.css') }}" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

</head>

<body class="external-page sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>
      <!-- Begin: Content -->
      <section id="content">

        <div class="admin-form theme-info mw600" style="margin-top: 13%;" id="login">
          <div class="row mb15 table-layout">

            <div class="col-xs-6 pln">
                <h4 class="Department_title">Login</h4>
            </div>

            <div class="col-xs-6 text-right va-b pr5">
              <div class="login-links">
                <a href="{{ route('password.request') }}" class="" title="False Credentials">Forget Password ?</a>
              </div>
            </div>
          </div>
          <div class="panel panel-info heading-border br-n">

            <!-- end .form-header section -->
            <form method="POST" action="{{ route('login') }}" id="contact">
                {{ csrf_field() }}

              <div class="panel-body bg-light pn">

                <div class="row table-layout">
                  <div class="col-xs-3 p20 pv15 va-m br-r bg-light">
                    <img class="br-a bw4 br-grey img-responsive center-block" src="{{ asset('admin-asset/admin-login/img/cart1.jpg') }}" title="Logo">
                  </div>
                  <div class="col-xs-9 p20 pv15 va-m bg-light">

                    <br>
                    <div class="section{{ $errors->has('email') ? ' has-error' : '' }}">
                      <label for="email" class="field prepend-icon">
                        <input type="email" name="email" id="email" class="gui-input" value="{{ old('email') }}" placeholder="Enter Email" required autofocus>
                        <label for="email" class="field-icon">
                          <i class="fa fa-user"></i>
                        </label>
                      </label>

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif

                    </div>
                    <!-- end section -->

                    <div class="section{{ $errors->has('password') ? ' has-error' : '' }} mt25">
                      <label for="password" class="field prepend-icon">
                        <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password" required>
                        <label for="password" class="field-icon">
                          <i class="fa fa-lock"></i>
                        </label>
                      </label>

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                    </div>
                    <!-- end section -->

                    <div class="clearfix ">
                        <input type="checkbox" id="inlineCheckbox1" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span>Remember me</span>
                   </div>

                  </div>
                </div>
              </div>

              <!-- end .form-body section -->
              <button style="margin-top:10px;" type="submit" class="button btn-info pull-right">Login</button>
            </form>
          </div>
         
        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->

  <script src="{{ asset('admin-asset/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin-asset/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>


  <!-- CanvasBG Plugin(creates mousehover effect) -->
  <script src="{{ asset('admin-asset/admin-login/js/canvasbg.js') }}" type="text/javascript"></script>


  <!-- Theme Javascript -->

  <script src="{{ asset('admin-asset/admin-login/js/utility.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin-asset/admin-login/js/demo.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin-asset/admin-login/js/main.js') }}" type="text/javascript"></script>


  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    // Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>





