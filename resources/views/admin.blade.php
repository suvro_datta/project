
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->

<head>
    <title>PosSoftware</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <!-- <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" /> -->
    
    <link rel="icon" type="image/png" href="{{ asset('images/logo/logo.png') }}" />

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="{{asset('admin-asset/bootstrap/dist/css/bootstrap.min.css')}}" />

    <link type="text/css" rel="stylesheet" href="{{asset('admin-asset/font-awesome/css/font-awesome.min.css')}}" />

    <link type="text/css" rel="stylesheet" href="{{asset('admin-asset/fonts/clip-font.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('admin-asset/iCheck/skins/all.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('admin-asset/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('admin-asset/css/main.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('admin-asset/css/main-responsive.min.css')}}" />
    <!-- <link type="text/css" rel="stylesheet" media="print" href="{{asset('admin-asset/css/print.min.css')}}" /> -->
    <link type="text/css" rel="stylesheet" id="skin_color" href="{{asset('admin-asset/css/theme/light.min.css')}}" />
    <!-- <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.slick/1.5.5/slick-theme.css"/>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.slick/1.5.5/slick.css"/> -->
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <script src="{{ asset('js/boot.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <?php 
        
    
    ?>

    <script>

        window.user = @json(
            [
                'user'  => auth()->user()->load('notifications'),
                'read_notifications'    => auth()->user()->readNotifications,
                'unread_notifications'  => auth()->user()->unreadNotifications
            ]
        );

        window.settings = @json([
            'settings'  => settings()
        ]);

    </script>

</head>

<body>

    <div id="loader" style="display:block"></div>

    <div id="content" style="display:none">

        <div id="fluentinventorypos">

            <!-- start: PRELOADER -->
            
            <!-- end: PRELOADER -->
            
            @auth
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endauth

            <!-- start: HEADER -->
            <div class="navbar navbar-inverse navbar-fixed-top">
                <!-- start: TOP NAVIGATION CONTAINER -->
                <div class="container">
                    <div class="navbar-header">
                        
                        <!-- start: RESPONSIVE MENU TOGGLER -->
                        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                            <span class="clip-list-2"></span>
                        </button>
                        <!-- end: RESPONSIVE MENU TOGGLER -->

                        <header-logo></header-logo>

                        <div class="clock" id="real-time-clock">
                            <datenow><datenow/>
                        </div>

                        <div class="navbar-tools">
                            <!-- start: TOP NAVIGATION MENU -->
                            <ul class="nav navbar-right">
                                <!-- start: NOTIFICATION DROPDOWN -->

                                <User></User>
                                
                                <!-- end: NOTIFICATION DROPDOWN -->

                            </ul>
                            <!-- end: TOP NAVIGATION MENU -->
                        </div>

                    </div>
                    
                </div>
                <!-- end: TOP NAVIGATION CONTAINER -->
            </div>
            <!-- end: HEADER -->

            <admin></admin>


        </div>
    
    </div>

    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
                <script src="../../bower_components/respond/dest/respond.min.js"></script>
                <script src="../../bower_components/Flot/excanvas.min.js"></script>
                <script src="../../bower_components/jquery-1.x/dist/jquery.min.js"></script>
                <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="{{asset('admin-asset/jquery/dist/jquery.min.js') }}"></script>

    <!--<![endif]-->
    <script type="text/javascript" src="{{ asset('admin-asset/jquery-ui/jquery-ui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('admin-asset/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-asset/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-asset/blockUI/jquery.blockUI.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-asset/iCheck/icheck.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-asset/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-asset/jquery.cookie/jquery.cookie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-asset/slick/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-asset/js/min/main.min.js') }}"></script>

    
    <!-- end: MAIN JAVASCRIPTS -->
    <script src="https://js.stripe.com/v3/"></script>
    
    <script>

        jQuery(document).ready(function() {
            Main.init();
        });


        $(window).load(function() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("content").style.display = "block";
        });


    </script>

    

    <!-- <script>
        function run() {
        const url = setUpQuery();
        fetch(url)
            .then(response => response.json())
            .then(json => {
            // See https://developers.google.com/speed/docs/insights/v5/reference/pagespeedapi/runpagespeed#response
            // to learn more about each of the properties in the response object.
            showInitialContent(json.id);
            const cruxMetrics = {
                "First Contentful Paint": json.loadingExperience.metrics.FIRST_CONTENTFUL_PAINT_MS.category,
                "First Input Delay": json.loadingExperience.metrics.FIRST_INPUT_DELAY_MS.category
            };
            showCruxContent(cruxMetrics);
            const lighthouse = json.lighthouseResult;
            const lighthouseMetrics = {
                'First Contentful Paint': lighthouse.audits['first-contentful-paint'].displayValue,
                'Speed Index': lighthouse.audits['speed-index'].displayValue,
                'Time To Interactive': lighthouse.audits['interactive'].displayValue,
                'First Meaningful Paint': lighthouse.audits['first-meaningful-paint'].displayValue,
                'First CPU Idle': lighthouse.audits['first-cpu-idle'].displayValue,
                'Estimated Input Latency': lighthouse.audits['estimated-input-latency'].displayValue
            };
            showLighthouseContent(lighthouseMetrics);
            });
        }

        function setUpQuery() {
        const api = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed';
        const parameters = {
            url: encodeURIComponent('https://developers.google.com')
        };
        let query = `${api}?`;
        for (key in parameters) {
            query += `${key}=${parameters[key]}`;
        }
        return query;
        }

        function showInitialContent(id) {
        document.body.innerHTML = '';
        const title = document.createElement('h1');
        title.textContent = 'PageSpeed Insights API Demo';
        document.body.appendChild(title);
        const page = document.createElement('p');
        page.textContent = `Page tested: ${id}`;
        document.body.appendChild(page);
        }

        function showCruxContent(cruxMetrics) {
        const cruxHeader = document.createElement('h2');
        cruxHeader.textContent = "Chrome User Experience Report Results";
        document.body.appendChild(cruxHeader);
        for (key in cruxMetrics) {
            const p = document.createElement('p');
            p.textContent = `${key}: ${cruxMetrics[key]}`;
            document.body.appendChild(p);
        }
        }

        function showLighthouseContent(lighthouseMetrics) {
        const lighthouseHeader = document.createElement('h2');
        lighthouseHeader.textContent = "Lighthouse Results";
        document.body.appendChild(lighthouseHeader);
        for (key in lighthouseMetrics) {
            const p = document.createElement('p');
            p.textContent = `${key}: ${lighthouseMetrics[key]}`;
            document.body.appendChild(p);
        }
        }

        run();
    </script> -->


</body>


</html>