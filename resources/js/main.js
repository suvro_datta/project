import moment from 'moment';

window.FluentInventoryPosBus = new window.FluentInventoryPos.Vue();

window.FluentInventoryPos.Vue.mixin({
    methods: {

        getNotifications(){

            let self=this;
            axios.get('/get-notifications')
                .then(response => {

                    this.read_notifications   = response.data.read_notifications;
                    this.unread_notifications = response.data.unread_notifications;

                    let notifications = {
                        'read_notifications':this.read_notifications,
                        'unread_notifications':this.unread_notifications
                    };

                    this.$store.dispatch('allNotifications',notifications);

                })
                .catch(error => {
                    console.log(error);
                })
                .then(() => {
                   //this.loading = false; 
                });

        },

        markAsRead(){
            let self=this;
            axios.get('/mark-as-read')
                .then(response => {

                    this.getNotifications();

                })
                .catch(error => {
                    console.log(error);
                })
                .then(() => {
                   //this.loading = false; 
                });
        },

        $dateFormat(date) {

            let format = 'MMM DD, YYYY';
            let dateString = (date === undefined) ? null : date;
            let dateObj = moment(dateString);
            return dateObj.isValid() ? dateObj.format(format) : null;

        },

        $dateFormatNow(date) {

            let format = 'YYYY-MM-DD';
            let dateString = (date === undefined) ? null : date;
            let dateObj = moment(dateString);
            return dateObj.isValid() ? dateObj.format(format) : null;

        },

        $dateFormatThree(dateParam) {
            if (!dateParam) {
              return null;
            }
          
            const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
            const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
            const today = new Date();
            const yesterday = new Date(today - DAY_IN_MS);
            const seconds = Math.round((today - date) / 1000);
            const minutes = Math.round(seconds / 60);
            const isToday = today.toDateString() === date.toDateString();
            const isYesterday = yesterday.toDateString() === date.toDateString();
            const isThisYear = today.getFullYear() === date.getFullYear();
          
          
            if (seconds < 5) {
              return 'now';
            } else if (seconds < 60) {
              return `${ seconds } seconds ago`;
            } else if (seconds < 90) {
              return 'about a minute ago';
            } else if (minutes < 60) {
              return `${ minutes } minutes ago`;
            } else if (isToday) {
              return this.getFormattedDate(date, 'Today'); // Today at 10:20
            } else if (isYesterday) {
              return this.getFormattedDate(date, 'Yesterday'); // Yesterday at 10:20
            } else if (isThisYear) {
              return this.getFormattedDate(date, false, true); // 10. January at 10:20
            }
          
            return this.getFormattedDate(date); // 10. January 2017. at 10:20
        },

        getFormattedDate(date, prefomattedDate = false, hideYear = false) {
            const day = date.getDate();
            const month = this.MONTH_NAMES[date.getMonth()];
            const year = date.getFullYear();
            const hours = date.getHours();
            let minutes = date.getMinutes();
          
            if (minutes < 10) {
              // Adding leading zero to minutes
              minutes = `0${ minutes }`;
            }
          
            if (prefomattedDate) {
              // Today at 10:20
              // Yesterday at 10:20
              return `${ prefomattedDate } at ${ hours }:${ minutes }`;
            }
          
            if (hideYear) {
              // 10. January at 10:20
              return `${ day }. ${ month } at ${ hours }:${ minutes }`;
            }
          
            // 10. January 2017. at 10:20
            return `${ day }. ${ month } ${ year }. at ${ hours }:${ minutes }`;
        },
        
        $post(url,data,headers='') {
            if (!data) {
                data = {};
            }
            if(!headers){
                headers = {}
            }
            return jQuery.post(url, {headers:{'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});

        },

        $get(url) {
            return jQuery.get(url);
        },

    },

    data(){
        return {
            unread_len:0,
            has_pro: 'no',
            pro_purchase_url: '#',
            currency_symbol:window.settings.settings.settings.general_settings.currency_settings.currency_symbol,
            unread_notifications:[],
            read_notifications:[],
            MONTH_NAMES : [
                'January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'
            ]
        }
    },

    watch:{
       
    },

    computed:{

        unreadNotifications(){
            return  this.$store.getters.getUnreadNotifications;
        },

        readNotifications(){
            return  this.$store.getters.getReadNotifications;
        },
        
    },

    
});

//routes

import {routes} from './routes'

const router = new window.FluentInventoryPos.Router({
    routes: routes,
    mode:'history',
    scrollBehavior() {
        return {x: 0, y: 0};
    },
    linkActiveClass: 'active'
});

router.beforeEach((to,from,next)=>{
    
    document.title = to.meta.title;
    if( from.path === '/admin/fluent/pos' ){
        window.open('/admin/',"_self");
    }else{
        next();
    }

});  

//vuex
import Vuex from 'vuex';

window.FluentInventoryPos.Vue.use(Vuex);

import storeFrontData from './store/index';
const store = new Vuex.Store(
    storeFrontData
);

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import Admin from './global/Admin';
import User from './global/User';
import DateNow from './global/Date';
import HeaderLogo from './global/header-logo';


window.FluentInventoryPos.Vue.component('Admin', Admin);
window.FluentInventoryPos.Vue.component('user', User);
window.FluentInventoryPos.Vue.component('datenow', DateNow);
window.FluentInventoryPos.Vue.component('HeaderLogo', HeaderLogo);

new window.FluentInventoryPos.Vue({
    el: '#fluentinventorypos',
    // render:h=>h(Admin),
    router:router,
    store,
    mounted() {
        
    }
});