export default{

	state:{

        customerData:{},
        customerNote:'',
        readNotifications:[],
        unreadNotifications:[],
        settings:{},
        user:'',
        advanceSettings:{}

    },

	getters:{

        getCustomerData(state){
            return state.customerData;
        },

        getCustomerNote(state){
            return state.customerNote;
        },

        getReadNotifications(state){
            return state.readNotifications;
        },

        getUnreadNotifications(state){
            return state.unreadNotifications;
        },

        getSettings(state){
            return state.settings;
        },

        getUser(state){
            return state.user;
        },

        getAdvanceSettings(){
            return state.advanceSettings;
        }

    },

    mutations:{

        setCustomerData(state,data){
            state.customerData = data;
        },

        setCustomerNote(state,data){
            state.customerNote = data;
        },

        setNotifications(state,data){
            state.readNotifications = data.read_notifications;
            state.unreadNotifications = data.unread_notifications;
        },

        setSettings(state,data){
            state.settings = data;
        },

        setUser(state,data){
            state.user = data;
        },

        setAdvanceSettings(state,data){
            state.advanceSettings = data;
        }

    },

	actions:{

        customerDataAction(context,data){
            context.commit('setCustomerData',data);
        },

        customerNoteAction(context,data){
            context.commit('setCustomerNote',data);
        },

        settingsAction(context,data){
            context.commit('setSettings',data);
        },

        allNotifications(context,data){
            context.commit('setNotifications',data);
        },

        userAction(context,data){
            context.commit('setUser',data);
        },

        advanceSettingsAction(context,data){
            context.commit('setAdvanceSettings',data);
        }

  }

};