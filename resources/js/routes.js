
import Dashboard from './pages/Dashboard';


//pos system
import Pos from './pages/pos-management/Pos';

//user management
import Users from './pages/user-management/Users';
import Roles from './pages/user-management/Roles';
import Permissions from './pages/user-management/Permissions';

//categories
import AllCategories from './pages/categories/AllCategories';
import AddCategory from './pages/categories/AddCategory';
import EditCategory from './pages/categories/EditCategory';

//brands
import AllBrands from './pages/brands/AllBrands';
import AddBrand from './pages/brands/AddBrand';
import EditBrand from './pages/brands/EditBrand';

//products
import AllProducts from './pages/products/AllProducts';
import AddProduct from './pages/products/AddProduct';
import EditProduct from './pages/products/EditProduct';

//orders
import AllOrders from './pages/orders/AllOrders';

//allCustomers
import AllCustomers from './pages/customers/AllCustomers';

//expenses
import AddExpense from './pages/expenses/AddExpense';
import AllExpenses from './pages/expenses/AllExpenses';
import EditExpense from './pages/expenses/EditExpense';

//settings
import Profile from './pages/settings/Profile';
import General from './pages/settings/General';
import PosSettings from './pages/settings/PosSettings';

//reports
import SalesExpenses from './pages/reports/SalesExpenses';

//social platforms 
import SocialPlatforms from './pages/socials/SocialPlatforms';
import SocialPlatformsSettings from './pages/socials/SocialPlatformsSettings';
import Sms from './pages/socials/Sms';
import Email from './pages/socials/Email';

//not found page
import PageNotFound from './pages/not-found/PageNotFound';

import Container from './global/Container';

const containerChildren = [
    {
        path: '/',
        component: Dashboard,
        meta:{title: 'Dashboard'}
    },
    //social platforms and sms
    {
        path: '/admin/social-platforms',
        component: SocialPlatforms,
        meta:{title: 'All Platforms'}
    },
    {
        path: '/admin/social-settings',
        component: SocialPlatformsSettings,
        meta:{title: 'Social and Sms Settings'}
    },
    {
        path: '/admin/sms',
        component: Sms,
        meta:{title: 'Sms'}
    },
    {
        path: '/admin/email',
        component: Email,
        meta:{title: 'Email'}
    },
    //products
    {
        path: '/admin/products',
        component: AllProducts,
        meta:{title: 'All Products'}
    },
    {
        path:'/admin/add-product',
        component: AddProduct,
        meta:{title: 'Add Product'}
    },
    {
        path:'/admin/edit-product/:product_id',
        component: EditProduct,
        meta:{title: 'Edit Product'}
    },

    //brands
    {
        path: '/admin/brands',
        component: AllBrands,
        meta:{title: 'All Brands'}
    },
    {
        path:'/admin/add-brand',
        component: AddBrand,
        meta:{title: 'Add Brand'}
    },
    {
        path:'/admin/edit-brand/:brand_id',
        component: EditBrand,
        meta:{title: 'Edit Brand'}
    },

    //categories
    {
        path: '/admin/categories',
        component: AllCategories,
        meta:{title: 'All Categories'}
    },
    {
        path:'/admin/add-category',
        component: AddCategory,
        meta:{title: 'Add Category'}
    },
    {
        path:'/admin/edit-category/:category_id',
        component: EditCategory,
        meta:{title: 'Edit Category'}
    },
    
    //user management
    // {
    //     path: '/admin/users',
    //     component: Users,
    //     meta:{title: 'Home'}
    // },
    // {
    //     path: '/admin/roles',
    //     component: Roles,
    //     meta:{title: 'Home'}
    // },
    // {
    //     path: '/admin/permissions',
    //     component: Permissions,
    //     meta:{title: 'Home'}
    // },

    //orders
    {
        path: '/admin/orders',
        component: AllOrders,
        meta:{title: 'All Orders'}
    },

    //customers
    {
        path: '/admin/customers',
        component: AllCustomers,
        meta:{title: 'All Customers'}
    },

    //expenses
    {
        path:'/admin/add-expense',
        component: AddExpense,
        meta:{title: 'Add Expense'}
    },
    {
        path:'/admin/expenses',
        component: AllExpenses,
        meta:{title: 'All Expenses'}
    },
    {
        path:'/admin/edit-expense/:expense_id',
        component: EditExpense,
        meta:{title: 'Edit Expense'}
    },

    //settings
    {
        path:'/admin/profile',
        component:Profile,
        meta:{title: 'Profile'}
    },
    {
        path:'/admin/general',
        component:General,
        meta:{title: 'General'}
    },
    {
        path:'/admin/pos-settings',
        component:PosSettings,
        meta:{title: 'Pos Settings'}
    },
    //reports
    {
        path:'/admin/reports',
        component:SalesExpenses,
        meta:{title: 'Sales Expenses'}
    },
    
];

export const routes = [

    {
        path: '/admin/',
        component: Container,
        children: containerChildren
    },

    {
        path: '/admin/fluent/pos',
        component: Pos,
        meta:{title: 'Pos'}
    },

    //not found page
    {
        path: "*", 
        component: PageNotFound,
        meta:{title: 'Not Found'} 
    }

];