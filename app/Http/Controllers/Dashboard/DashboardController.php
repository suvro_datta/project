<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Model\Expense;
use App\Model\Order;

use DB;

class DashboardController extends Controller
{
    public function dashboardInfo(){

        $totalExpense   = Expense::whereDate('created_at', Carbon::today())
                        ->sum('total_expense');

        $totalSell      = Order::whereDate('created_at', Carbon::today())
                        ->sum('paid');

        $totalOrders    = Order::whereDate('created_at', Carbon::today())->count();

        //get last 30 days income 
        $fromToOrders   =   DB::table('orders')
                            ->select(
                                DB::raw('DATE(created_at) as date'),
                                DB::raw('SUM(paid) as sum')
                            )
                            ->whereDate('created_at', '>', Carbon::now()->subDays(30))
                            ->groupBy('date')
                            ->get();

        //get last 30 days expense 
        $fromToExpenses   =   DB::table('expenses')
                            ->select(
                                DB::raw('DATE(created_at) as date'),
                                DB::raw('SUM(total_expense) as sum')
                            )
                            ->whereDate('created_at', '>', Carbon::now()->subDays(30))
                            ->groupBy('date')
                            ->get();

        $monthlyIncomes = array();
        $monthlyExpenses   = array();
        $dates = array();
        
        if( !empty($fromToOrders) ){
        
            
            $incomes    = array();
            $expenses   = array();

            //last 30 days
            $today     = new \DateTime(); // today
            $begin     = $today->sub(new \DateInterval('P15D')); //created 30 days interval back
            $end       = new \DateTime();
            $end       = $end->modify('+1 day'); // interval generates upto last day
            $interval  = new \DateInterval('P1D'); // 1d interval range
            $daterange = new \DatePeriod($begin, $interval, $end); // it always runs forwards in date
            
            foreach ($daterange as $date) { // date object
                $thirtyDays[] = $date->format("Y-m-d"); // your date
            }

            foreach($fromToOrders as $orderData){
                $incomes[$orderData->sum] = $orderData->date;
            }

            foreach($fromToExpenses as $expenseData){
                $expenses[$expenseData->sum] = $expenseData->date;
            }

            foreach( $thirtyDays as $date ){

                $income = array_search($date,$incomes,true);
                $expense = array_search($date,$expenses,true);

                $dateCreate  = date_create($date);
                $dateNow = date_format($dateCreate,"d M y");
                array_push($dates,$dateNow);

                if(!$income){
                   array_push($monthlyIncomes,0); 
                }else{
                    array_push($monthlyIncomes,$income);
                }

                if(!$expense){
                    array_push($monthlyExpenses,0); 
                 }else{
                     array_push($monthlyExpenses,$expense);
                 }

            }

        }

        return response()->json([

            'message'       => 'dashboard data found!',
            'totalExpense'  =>  $totalExpense,
            'totalIncome'   =>  $totalSell,
            'totalOrders'   =>  $totalOrders,
            'totalProfit'   =>  ($totalSell-$totalExpense),

            //last 30 days income
            'monthlyIncomes'    => $monthlyIncomes,
            'dates'             => $dates,
            'monthlyExpenses'    => $monthlyExpenses

        ]);

    }

    public function timelineIncomeExpense($days){

        $response = false;

        if($days === 'daily'){

           $response =  $this->getDailyInfo();

        }

        else if( $days === 'weekly' ){

            $response = $this->getWeeklyInfo();

        }

        else if( $days === 'monthly' ){

            $response =  $this->getMonthlyInfo();
            
        }

        else if( $days === 'yearly' ){

            $response =  $this->getYearlyInfo();
            
        }

        return response()->json(

            $response,
            200

        );

    }

    public function getDailyInfo() {

        $totalExpense   = Expense::whereDate('created_at', Carbon::today())->sum('total_expense');
        $totalIncome    = Order::whereDate('created_at', Carbon::today())->sum('paid');
        $totalOrders    = Order::whereDate('created_at', Carbon::today())->count();

        return array(
            'totalIncome'   => $totalIncome,
            'totalExpense'  => $totalExpense,
            'totalOrders'   => $totalOrders,
            'totalProfit'   => ($totalIncome-$totalExpense)
        );

    }

    public function getWeeklyInfo() {

        $totalIncome  = Order::whereDate('created_at','>=',Carbon::today()->subDays(7))->sum('paid');
        $totalExpense = Expense::whereDate('created_at', '>=', Carbon::today()->subDays(7))->sum('total_expense');
        $totalOrders  = Order::whereDate('created_at', Carbon::today()->subDays(7))->count();
        
        return array(
            'totalIncome'   => $totalIncome,
            'totalExpense'  => $totalExpense,
            'totalOrders'   => $totalOrders,
            'totalProfit'   => ($totalIncome-$totalExpense)
        );

    }

    public function getMonthlyInfo() {

        $totalIncome  = Order::whereMonth('created_at', Carbon::now()->month)->sum('paid');
        $totalExpense = Expense::whereMonth('created_at', Carbon::now()->month)->sum('total_expense');
        $totalOrders  = Order::whereMonth('created_at', Carbon::now()->month)->count();

        return array(
            'totalIncome'   => $totalIncome,
            'totalExpense'  => $totalExpense,
            'totalOrders'   => $totalOrders,
            'totalProfit'   => ($totalIncome-$totalExpense)
        );

    }

    public function getYearlyInfo(){

        // $months = DB::table('times')->whereYear('start_day', (string) Carbon::now()->year)->get();


        $totalIncome  = Order::whereYear('created_at', Carbon::now()->year)->sum('paid');
        $totalExpense = Expense::whereYear('created_at', Carbon::now()->year)->sum('total_expense');
        $totalOrders  = Order::whereYear('created_at', Carbon::now()->year)->count();

        return array(
            'totalIncome'   => $totalIncome,
            'totalExpense'  => $totalExpense,
            'totalOrders'   => $totalOrders,
            'totalProfit'   => ($totalIncome-$totalExpense)
        );

    }
}
