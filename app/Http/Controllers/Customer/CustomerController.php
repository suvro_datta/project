<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Customer;

class CustomerController extends Controller
{
    public function index() {
        
        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search  = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $Query   = Customer::with(['orders']);

        // if (!empty($search)) {

        //     $Query->whereHas('orders', function ($query) use ($search) {

        //         // $query->where('order_status', 'LIKE', "%{$search}%");
        //         // $query->orWhere('paid', 'LIKE', "%{$search}%");
        //         $query->orWhere('subtotal', 'LIKE', "%{$search}%");
        //         // $query->orWhere('tax', 'LIKE', "%{$search}%");
        //         // $query->orWhere('shipping', 'LIKE', "%{$search}%");
        //         // $query->orWhere('discount', 'LIKE', "%{$search}%");
        //         // $query->orWhere('id', 'LIKE', "%{$search}%");
                
        //     });

        // }

        if (!empty($search)) {

            $Query->where('id', 'LIKE', "%{$search}%");
            $Query->orWhere('phone', 'like', '%'.$search.'%');
            $Query->orWhere('email', 'like', '%'.$search.'%');
            $Query->orWhere('name', 'like', '%'.$search.'%');
            $Query->orWhere('description', 'like', '%'.$search.'%');
            $Query->orWhere('home_address', 'like', '%'.$search.'%');
            $Query->orWhere('city', 'like', '%'.$search.'%');
            $Query->orWhere('country', 'like', '%'.$search.'%');

          
        }

        $total           = $Query->count();
        $all_customers   = $Query->orderBy('id', 'desc')->paginate($paginate);

        return response()->json([

            'message' => 'Customer Created!',
            'customers'=>$all_customers,
            'total' => $total,
            'search'=>$search

        ]);

    }
}
