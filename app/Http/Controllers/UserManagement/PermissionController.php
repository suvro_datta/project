<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function deleteMultiple(Request $request){

    	$rows= $request->row_ids;

    	foreach ($rows as $row) {
    		$id = intval($row['id']);
    		Permission::destroy($id);
    	}

    	return response()->json(['message' =>'Data Deleted']);
    	
    }

    public function getPermissions(){
    	
    	$all_permissions = Permission::all();
    	return response()->json(['message' => 'Permission Created!','permissions'=>$all_permissions]);

    }

}
