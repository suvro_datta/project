<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use App\Http\Resources\UserResource;
use App\Http\Resources\RoleResource;
use App\User;

use Spatie\Activitylog\Models\Activity;


//added by me
use Spatie\Permission\Models\Permission;


class RoleController extends Controller
{
	private $role;

	public function __construct(Role $role){
        $this->role = $role;
    }

    public function index(){



        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search   = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $roleQuery   = $this->role;

        if (!empty($search)) {

             $roleQuery = $roleQuery->where('name', 'LIKE', "%{$search}%");
             $roleQuery->orWhere('id', 'LIKE', "%{$search}%");
          
        }

        $total       = $roleQuery->count();
        $all_roles   = $roleQuery->orderBy('id', 'desc')->paginate($paginate);

        return response()->json(['message' => 'Role Created!','roles'=>RoleResource::collection($all_roles),
            'total' => $total,'search'=>$search]);

    }

    public function create(Request $request){

    	$request->validate([
           'name'=>'required',
           'permissions'=>'required|array'
        ]);

        $role= $this->role->create([
            'name'=> $request->name
        ]);
        
        activity()
            ->performedOn(new Role())
            ->causedBy(auth()->user())
            ->withProperties([
                'name'=> $request->name
            ])->log('created');


        if ($request->has('permissions')) {

            $role->givePermissionTo(Permission::select('id')->whereIn('name',$request->permissions)->pluck('id')->toArray());

        }

        return response(['message'=>'Role Created']);

    }


    public function update(Request $request, Role $role)
    {
        // $request->validate([
        //     'name'=>'required',
        // ]);

        // $role->update([
        //     'name'=> $request->name,
        // ]);
        // activity()
        //     ->performedOn(new Role())
        //     ->causedBy(auth()->user())
        //     ->withProperties([
        //         'name'=> $request->name
        //     ])
        //     ->log('updated');
        // if ($request->has('permissions')) {
        //     $role->syncPermissions(collect($request->permissions)->pluck('id')->toArray());
        // }

        return response(['message'=>'Role Updated']);

    }
    
}
