<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;

//added by me
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    public function index(){

        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search   = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $roleQuery   = User::select('*');

        if (!empty($search)) {

             $roleQuery = $roleQuery->where('name', 'LIKE', "%{$search}%");
             $roleQuery->orWhere('id', 'LIKE', "%{$search}%");
          
        }

        $total       = $roleQuery->count();
        $all_roles   = $roleQuery->orderBy('id', 'desc')->paginate($paginate);

        return response()->json(['message' => 'All Created!','users' => UserResource::collection($all_roles),
            'total' => $total,'search'=>$search]);

    }
    
    public function create(Request $request){

        try {

            $request->validate([
                'name'=>'required',
                'email'=>'required|email',
                'password'=>'required|confirmed',
                'role' => 'required',
                'permissions' => 'required|array',
                'password_confirmation'  => 'required'
            ]);

            $user= User::create([
                'name'=> $request->name,
                'email'=> $request->email,
                'password'=> bcrypt($request->password)
            ]);

            if ($request->has('role')) {
                $user->assignRole($request->role);
            }

            if ($request->has('permissions')) {
                $user->givePermissionTo(Permission::select('id')->whereIn('name',$request->permissions)->pluck('id')->toArray());
            }

            return response()->json([
                'status' => 'success',
                'message'=>'User Created', 
                'user'=>$user
            ],201);

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }

    public function update(Request $request,$id)
    {
        
        try{

            //return response()->json(['message'=>'User Updated','request'=>$request->all(), ],201);
            
            $request->validate([
                'name'=>'required',
                'email'=>'required|email',
                //'password'=>'required|confirmed',
                'role' => 'required',
                'permissions' => 'required|array',
                //'password_confirmation'  => 'required'
            ]);

           // $request->password = '123456';

            $user = User::where('id',$id)->update([
                'name'  => $request->name,
                'email' => $request->email,
            ]);
            
            if($user){

                $user = User::where('id',$id)->first();

                if ($request->has('role')) {
                    $user->syncRoles($request->role);
                }

                if ($request->has('permissions')) {
                    $user->syncPermissions(Permission::select('id')->whereIn('name',$request->permissions)->pluck('id')->toArray());
                }

                return response()->json(['message'=>'User Updated','request'=>$request->all(),'id'=>$id,'user'=>$user ],201);
            }
            
            // $user = User::update([
            //     'name'=> $request->name,
            //     'email'=> $request->email,
            //     'password'=> bcrypt($request->password)
            // ])->where('id',$request->id);

            

        }catch (ValidationException $exception) {

            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);

        }


        

        

        // return response(['message'=>'User Updated', 'user'=>$user]);

    }

    public function delete($id){

        User::destroy($id);

        return response()->json([
            'messege'   => 'Data deleted!',
            'status'    => 'OK'
        ],201);

    }
}
