<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;

use App\Model\Product;
use App\Model\Brand;
use App\Model\Category;

class ProductController extends Controller
{
    public function index() {

        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search   = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $Query   = Product::with(['category','brand']);

        if(!empty($search)) {

            $Query =    $Query->whereHas('category', function ($query) use ($search){
                            $query->where('name', 'like', '%'.$search.'%');
                        });

        }

        if(!empty($search)) {

            $Query =    $Query->orWhereHas('brand', function ($query) use ($search){
                            $query->where('name', 'like', '%'.$search.'%');
                        });

        }
    
        if (!empty($search)) {

            $Query = $Query->orWhere('name', 'LIKE', "%{$search}%");
            $Query->orWhere('id', 'LIKE', "%{$search}%");
            $Query->orWhere('barcode', 'LIKE', "%{$search}%");
            $Query->orWhere('description', 'LIKE', "%{$search}%");
          
        }

        $total            = $Query->count();
        $all_products     = $Query->orderBy('id', 'desc')->paginate($paginate);

        return response()->json([

            'message' => 'Product Created!',
            'products'=>$all_products  ,
            'total' => $total,
            'search'=>$search

        ]);

    }

    public function allCategoriesAndProducts(){

        $categories = Category::all();
        $brands     = Brand::all();

        return response()->json([

            'status' => 'success',
            'msg'    => 'Categories and products found!',
            'categories'    => $categories,
            'brands'        => $brands

        ], 200);
    }

    public function create(Request $request){

        try{

            $request->validate([

                'name'          =>'required',
                'images'        => 'array|required',
                'price'         =>'required|numeric',
                'description'   =>'required',
                'featured'      =>'required',
                'quantity'      =>'required|numeric',
                'category'      =>'required',
                'brand'         =>'required'

            ]);

            $brand_id       = Brand::select('id')->where('name', '=', $request->brand)->first();
            $category_id    = Category::select('id')->where('name', '=', $request->category)->first();
            
            //image upload in public folder
            $images = [];
            if (count($request->images)) {
                foreach ($request->images as $image) {
                                        
                    //full image name with extension
                    $image_name_with_ext = $image->getClientOriginalName();

                    //image name without extension
                    $image_name_without_ext = pathinfo($image_name_with_ext,PATHINFO_FILENAME);

                    //get extension of that image
                    $image_ext = $image->getClientOriginalExtension();

                    //image name with encription
                    $image_name_to_store = $image_name_without_ext.'_'.time().'_.'.$image_ext;

                    //public folder
                    $path = public_path('/images/products');
                    $image->move($path,$image_name_to_store);

                    $images[]=[
                       'name'   => $image_name_to_store
                    ];

                }
            }

            //process fails
            // Product::create([

            //     'name'          =>$request->name,
            //     'category_id'   =>$category_id->id,
            //     'brand_id'      =>$brand_id->id,
            //     'images'        =>json_encode($request->images),
            //     'description'   =>$request->description,
            //     'featured'      =>$request->featured,
            //     'quantity'      =>$request->quantity,
            //     'price'         =>$request->price,
            //     'slug'          => $request->name,
            //     'image'     => 'image',
            //     'details'   =>'details',

            // ]);

            $product = new Product;

            $product->name         = $request->name;
            $product->category_id  = $category_id->id;
            $product->brand_id     = $brand_id->id;
            $product->images       = json_encode($images);
            $product->description  = $request->description;
            $product->featured     = ($request->featured == 'true');
            $product->quantity     = $request->quantity;
            $product->price        = $request->price;
            $product->barcode      = $request->barcode;

            $product->save();
           
            return response()->json([
                'message'       => 'Product created',
                //'data'          => $request->all(),
                'files'         => $request->images,
              
                'image'     => 'image',
                'details'   =>'details',
                'status'        => 'success'
            ],201);
        

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }

    public function update(Request $request,$id){

        try{

            $request->validate([

                'name'          =>'required',
                //'images'        => 'required',
                'price'         =>'required|numeric',
                'description'   =>'required',
                'featured'      =>'required',
                'quantity'      =>'required|numeric',
                'category'      =>'required',
                'brand'         =>'required'

            ]);

            $brand_id       = Brand::select('id')->where('name', '=', $request->brand)->first();
            $category_id    = Category::select('id')->where('name', '=', $request->category)->first();
            
            //image upload in public folder

            $imagesLength = isset($request->images)?count($request->images):0;
            
            $product = Product::find($id);
            $images = $product->images;

            if( $imagesLength ){

                $images = [];
                if (count($request->images)) {
                    foreach ($request->images as $image) {
                                            
                        //full image name with extension
                        $image_name_with_ext = $image->getClientOriginalName();

                        //image name without extension
                        $image_name_without_ext = pathinfo($image_name_with_ext,PATHINFO_FILENAME);

                        //get extension of that image
                        $image_ext = $image->getClientOriginalExtension();

                        //image name with encription
                        $image_name_to_store = $image_name_without_ext.'_'.time().'_.'.$image_ext;

                        //public folder
                        $path = public_path('/images/products');
                        $image->move($path,$image_name_to_store);

                        $images[]=[
                        'name'   => $image_name_to_store
                        ];

                    }
                }

                $images = json_encode($images);

            } 

            //process fails
            $product->update([

                'name'          =>$request->name,
                'category_id'   =>$category_id->id,
                'brand_id'      =>$brand_id->id,
                'images'        =>$images,
                'description'   =>$request->description,
                'featured'      =>($request->featured == 'true'),
                'quantity'      =>$request->quantity,
                'price'         =>$request->price,
                'slug'          => $request->name,
                'image'         => 'image',
                'details'       =>'details',
                'barcode'       =>$request->barcode,

            ]);

            // $product = Product::find($id);

            // $product->name         = $request->name;
            // $product->category_id  = $category_id->id;
            // $product->brand_id     = $brand_id->id;
            // // $product->images       = json_encode($images);
            // $product->description  = $request->description;
            // $product->featured     = ($request->featured == 'true');
            // $product->quantity     = $request->quantity;
            // $product->price        = $request->price;

            // $product->update();
            
            

            return response()->json([
                'message'       => 'Product created',
                'data'          => $request->all(),
                'files'         => $product->images,
              
                'image'     => 'image',
                'details'   =>'details',
                'status'    => 'success',

            ],201);
        

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

        // 

        // return response()->json([

        //     'status' => 'success',
        //     'id'     => $id,
        //     'res'    => $request->all(),
        //     'image'  => $imagesLength

        // ],201);
    }

    public function edit($id){

        $categories = Category::all();
        $brands     = Brand::all();
        $products   = Product::with(['category','brand'])->find($id);

        return response()->json([

            'status' => 'success',
            'msg'    => 'Products found',
            'categories'    => $categories,
            'brands'        => $brands,
            'products'      => $products

        ], 200);

    }

    public function delete($id){
        return Product::destroy($id);
    }

    public function deletemultiple(Request $request){
        
        if( $request->has('rows') ){

            Product::destroy(collect($request->rows)->pluck('id')->toArray());

            return response()->json([

                'status'    => 'success',
                'message'   => 'Data deleted!',
                'data'      => $request->rows
    
            ],201);

        }

    }
}
