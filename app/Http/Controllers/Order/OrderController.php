<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Order;

class OrderController extends Controller
{
    public function index() {
        
        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search  = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $Query   = Order::with(['customer']);

        //search relational database customer
        if(!empty($search)) {

            $Query =    $Query->whereHas('customer', function ($query) use ($search){

                        $query->where('phone', 'like', '%'.$search.'%');
                        $query->orWhere('email', 'like', '%'.$search.'%');
                        $query->orWhere('name', 'like', '%'.$search.'%');
                        $query->orWhere('description', 'like', '%'.$search.'%');
                        $query->orWhere('home_address', 'like', '%'.$search.'%');
                        $query->orWhere('city', 'like', '%'.$search.'%');
                        $query->orWhere('country', 'like', '%'.$search.'%');

                    });

        }

        //search order table
        if (!empty($search)) {

            $Query = $Query->orWhere('order_status', 'LIKE', "%{$search}%");
            $Query->orWhere('paid', 'LIKE', "%{$search}%");
            $Query->orWhere('subtotal', 'LIKE', "%{$search}%");
            $Query->orWhere('tax', 'LIKE', "%{$search}%");
            $Query->orWhere('shipping', 'LIKE', "%{$search}%");
            $Query->orWhere('discount', 'LIKE', "%{$search}%");
            $Query->orWhere('id', 'LIKE', "%{$search}%");
          
        }

        $total        = $Query->count();
        $all_orders   = $Query->orderBy('id', 'desc')->paginate($paginate);

        return response()->json([

            'message' => 'Order Created!',
            'orders'=>$all_orders,
            'total' => $total,
            'search'=>$search

        ]);

    }
}
