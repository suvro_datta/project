<?php

namespace App\Http\Controllers\Pos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Cart;
use App\Model\Product;
use Throwable;

class CartController extends Controller
{
    public function updateCart(Request $request) {

        $cartProduct = Cart::get($request->id);
        
        if(!$cartProduct || empty($cartProduct) && $request->quantity ){
            
            Cart::add(array(

                'id'        =>  $request->id,
                'name'      =>  $request->name,
                'price'     =>  $request->price,
                'quantity'  =>  $request->quantity,
                'attributes'=>  array(
                    'id'    =>  $request->id,
                    'images'    =>  $request['attributes']['images'],
                )
    
            ));   

        }
        
        else{

            Cart::remove($request->id);

            Cart::add(array(

                'id'        => $request->id,
                'name'      =>  $request->name,
                'price'     =>  $request->price,
                'quantity'  =>  $request->quantity,
                'attributes'=>  array(
                    'id'    =>  $request->id,
                    'images'    =>  $request['attributes']['images'],
                )

            ));

        }

        //$cartCollection = Cart::getContent();

        return response()->json([
    
            'message'        =>  'Cart Products updated',
            // 'request'        => $request['attributes']['images'],
            // 'cartItems'     =>  $cartCollection,
            // 'cartItem'     =>  Cart::get($request->id),
            // 'total'         =>  $cartCollection->count(),
            // 'cartTotal'     =>  Cart::getTotal(),
            // 'cartSubTotal'  =>  Cart::getSubTotal(),
            'status'        =>  'success',

        ],201);

    }

    //remove all item from cart
    public function deleteCart(){

        Cart::clear();

        //$cartCollection = Cart::getContent();

        return response()->json([

            'message'       =>  'Cart Products',
            // 'cartItems'     =>  $cartCollection,
            // 'total'         =>  $cartCollection->count(),
            // 'cartTotal'     =>  \Cart::getTotal(),
            // 'cartSubTotal'  =>  \Cart::getSubTotal(),
            // 'read_notifications'    => auth()->user()->readNotifications,
            // 'unread_notifications'  => auth()->user()->unreadNotifications,
            'status'        =>  'success',

        ],201);

    }

    //remove specific item from cart
    public function removeItemFromCart(Request $request) {

        $cartItem = json_decode(json_encode($request->all(),true));

        Cart::remove( $cartItem->id );

        return response()->json([

            'message'   => 'Item remove from cart',
            'cartItem'  => $cartItem

        ],200);

    }

    public function cartProducts(){

        $cartCollection = Cart::getContent();

        return response()->json([

            'message'       =>  'Cart Products',
            'cartItems'     =>  $cartCollection,
            'total'         =>  $cartCollection->count(),
            'cartTotal'     =>  Cart::getTotal(),
            'cartSubTotal'  =>  Cart::getSubTotal(),
            'status'        =>  'success',

        ],201);

    }

    public function cartProduct(Request $request){

        $cartProduct = Cart::get($request->id);
        
        if(!$cartProduct || empty($cartProduct)){

            $cartProduct = array(
                'id'        => $request->id,
                'name'      => $request->name,
                'quantity'  => 0,
                'price'     => $request->price,
                'attributes'=>  array(
                    'id'    =>  $request->id,
                    'images'    =>  $request->images,
                )
            );

        }

        return response()->json([

            'message'       =>  'Single cart product',
            'cartItem'      =>  $cartProduct ,
            'status'        =>  'success'

        ],201);

    }

    public function addToCart(Request $request){

        Cart::add(array(

            'id'        =>  $request->id,
            'name'      =>  $request->name,
            'price'     =>  $request->price,
            'quantity'  =>  $request->qty,
            'attributes'=>  array(
                'id'    =>  $request->id,
                'images'    =>  $request->images,
            )

        ));

        // $cartCollection = Cart::getContent();
        // $cartProduct = Cart::get($request->id);

        return response()->json([

            'message'       =>  'Item added to cart',
            // 'cartItems'     =>  $cartCollection,
            // 'cartItem'      =>  Cart::get($request->id),
            // 'total'         =>  $cartCollection->count(),
            // 'cartTotal'     =>  Cart::getTotal(),
            // 'cartSubTotal'  =>  Cart::getSubTotal(),
            // 'added'         =>  $cartProduct,
            // 'request'       =>  $request->images,
            'status'        => 'success'

        ],201);

    }

    public function addToCartByBarcode(Request $request){

        $barcode = $request->barcode;
        $qty     = $request->qty;

        $errors = [];

        if(empty($qty)){
            if($qty==='0'){
                $errors['qty'][] = 'Quantity can\'t be 0!';
            }else{
                $errors['qty'][] = 'Quantity is required!';
            }
        }

        if(empty($barcode)){
            $errors['barcode'][] = 'Barcode is required!';
        }

        if(empty($barcode) || empty($qty)){
            return response()->json([
                'message'       => 'error',
                'errors'        => $errors
            ],423);
        }

        try{
            $product = Product::where('barcode',$barcode)->first();

            if(!empty($product)){

                Cart::add(array(

                    'id'        =>  $product->id,
                    'name'      =>  $product->name,
                    'price'     =>  $product->price,
                    'quantity'  =>  intVal($qty),
                    'attributes'=>  array(
                        'id'    =>  $product->id,
                        'images'    =>  $product->images,
                    )
        
                ));

               
            }

            return response()->json([

                'message'       =>  'Item added to cart',
                'status'        =>  'success',
                'product'       =>  $product

            ],201);

        }catch (Throwable $e) {

            $error =  $e->getMessage();
            return response()->json([

                'status'        =>  'error',
                'errors'        =>  $error

            ],423);

        }

    }
}
