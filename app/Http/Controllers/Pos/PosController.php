<?php

namespace App\Http\Controllers\Pos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use Throwable;

use App\Model\Product;
use App\Model\Brand;
use App\Model\Category;

use App\Model\Customer;
use App\Model\Order;
use App\Model\OrdersProducts;

use App\Notifications\OrderCompleted;
use Illuminate\Support\Facades\Auth;
use App\User;
use Cart;

use Cartalyst\Stripe\Stripe;

class PosController extends Controller
{
    public function getInitialPosData(){

        $categories     = Category::all();
        $products       = Product::all();
        $cartCollection = Cart::getContent();

        return response()->json([

            'status'        =>  'success',
            'message'       =>  'initital pos data found!',
            'categories'    =>  $categories,
            'products'      =>  $products,
            'cartItems'     =>  $cartCollection,
            'total'         =>  $cartCollection->count(),
            'cartTotal'     =>  Cart::getTotal(),
            'cartSubTotal'  =>  Cart::getSubTotal(),

        ],200);
    }

    public function addCustomer($customerDetails){

        $customer;

        if( $customerDetails['customer_type'] === 'regular_customer' ){
           
            $customer = Customer::select('*')->where('phone',$customerDetails['phone_number'])->first();
            
            if($customer){

                $customer = Customer::where('id', $customer->id)->update([

                    'customer_type' =>  $customerDetails['customer_type'],
                    'phone'         =>  $customerDetails['phone_number'],
                    'email'         =>  $customerDetails['email_address'],
                    'name'          =>  $customerDetails['customer_name'],
                    'description'   =>  $customerDetails['description'],
                    'home_address'  =>  $customerDetails['home_address'],
                    'city'          =>  $customerDetails['city'],
                    'country'       =>  $customerDetails['country']

                ]);

                $customer = Customer::select('*')->where('phone',$customerDetails['phone_number'])->first();
                $customer = $customer->id;

            }
            else{
                $customer = Customer::create([

                    'customer_type' =>  $customerDetails['customer_type'],
                    'phone'         =>  $customerDetails['phone_number'],
                    'email'         =>  $customerDetails['email_address'],
                    'name'          =>  $customerDetails['customer_name'],
                    'description'   =>  $customerDetails['description'],
                    'home_address'  =>  $customerDetails['home_address'],
                    'city'          =>  $customerDetails['city'],
                    'country'       =>  $customerDetails['country'],
    
                ]);

                $customer = $customer->id;
            }
        
        }else{

            $customer = Customer::create([

                'customer_type' =>  $customerDetails['customer_type'],

            ]);
            $customer = $customer->id;

        }

        return $customer;

    }

    public function addOrder($orderDetails,$customer_id){

        $authUser = Auth::user()->id;
        User::find($authUser)->notify(new OrderCompleted);

        $order = Order::create([

           'customer_id'    =>  $customer_id,
           'order_status'   =>  $orderDetails['order_status'],
           'paid'           =>  $orderDetails['paid'],
           'subtotal'       =>  $orderDetails['subtotal'],
           'tax'            =>  $orderDetails['tax'],
           'shipping'       =>  $orderDetails['shipping'],
           'discount'       =>  $orderDetails['discount'],

        ]);

        return $order->id;

    }

    public function orderProduct($orderedProducts,$orderId){

        foreach($orderedProducts as $orderedProduct ){

            OrdersProducts::create([

                'order_id'      => $orderId,
                'product_id'    => $orderedProduct['id'],
                'quantity'      => $orderedProduct['quantity']

            ]);

        }

    }

    public function savePosData(Request $request){

        $payment_method = $request->payment_method;

        if( $payment_method === 'debit_card' || $payment_method === 'credit_card' ) {
            $secret  = $request->stripeSecret;//'sk_test_51HAXwSH5wL85b1wT5fRkmrxCaN1jqTqxiXIPC15lts55GKbOQcLuDn7W2w0ozoYMZxYf4aSI6RanjD4b8jebaWLS009uFTJR4j';
            $version = '2019-02-19';
            $stripe = new Stripe($secret, $version);
            
            $charge = $stripe->charges()->create([

                'amount'        => 20,
                'currency'      => 'CAD',
                'source'        => $request->stripeToken,
                'description'   => 'Description goes here',
                'receipt_email' => 'suvrodatta85@gmail.com',
                'metadata' => [
                    'data1' => 'metadata 1',
                    'data2' => 'metadata 2',
                    'data3' => 'metadata 3',
                ],

            ]);

            return response()->json([

                'message'   => 'Payment Successfully Made!',
                'charge'    => $charge

            ],200);

        }
        
        //add customer first
        $customerDetails = $request->cutomerDetails;
        $customerId = $this->addCustomer($customerDetails);

        //add order secondly
        $orderDetails   = $request->orderDetails;
        $orderId        = $this->addOrder($orderDetails,$customerId);

        //add products associated with this order
        $orderedProducts    = $request->orderedProducts;
        $orderProducts      = $this->orderProduct($orderedProducts,$orderId); 

        return response()->json([

            'message'           => 'customer not exists',
            'data'              => $request->all(),
            'id'                => $customerId,
            'orderDetails'      => $orderDetails['order_status'],
            'orderId'           => $orderId,
            'orderedProducts'   => $orderedProducts,

        ],201);

    }

    public function findUser(Request $request) {

        $customer = Customer::select('*')->where('phone',$request->phone_number)->first();

        return response()->json([

            'message'   => 'customer not exists',
            'customer'      => $customer

        ],201);       

    }

    public function getCategoryProducts() {

        $productsByCategory = array();

        $categoryId = isset($_REQUEST['categoryId'])?$_REQUEST['categoryId']:-1;
        $search     = isset($_REQUEST['s'])?$_REQUEST['s']:'';
        
        if (!empty($search)) {

            $Query = Product::with(['category']); 
            $Query = $Query->where('name', 'LIKE', "%{$search}%");
            $productsByCategory = $Query->get();
          
        }else{

            if($categoryId == -1) {

                $productsByCategory = Product::with(['category'])->get(); 

            }else {

                $productsByCategory = Product::with(['category'])->where('category_id',$categoryId)->get(); 

            }
            
        }

        return response()->json([

            'message'   => 'Category Products Found!',
            'products'  => $productsByCategory,
            'status'    => 'success'

        ],201);


    }

}
