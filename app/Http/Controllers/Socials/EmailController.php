<?php

namespace App\Http\Controllers\Socials;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Throwable;

use App\Model\Customer;
use App\Model\SocialSettings;
use Illuminate\Validation\ValidationException;

class EmailController extends Controller
{
    function getEmailsWithEmailSettings() {

        $customersEmail   = Customer::select('email')->where('customer_type','regular_customer')->get();
        $emailSettings    = SocialSettings::select('email_settings')->first();;

        $arr = json_decode($customersEmail, true);

        $emailLists = array_column($arr, 'email');
        
        return response()->json([

            'message'       => 'customers email and email settings found successfully',
            'customers'     => $customersEmail,
            'emailSettings' => $emailSettings,
            'emailLists'    => $emailLists

        ],200);

    }

    function sendEmail(Request $request) {
        
        $errors = [];

        if(empty($request->mailgunDomain)){
            $errors['domain'][] = 'mailgun domain not found';
        }

        if(empty($request->mailgunAuthToken)){
            $errors['token'][] = 'maingun auth token not found';
        }

        if( empty($request->mailgunDomain) || empty($request->mailgunAuthToken) ){
            
            return response()->json([
                'message'       => 'error',
                'errors'        => $errors
            ],423);
            
        }

        try {

            $domain     = $request->mailgunDomain;
            $api_key    = $request->mailgunAuthToken;
            $from       = $request->emailFrom;

            $emails = json_encode($request->emails);
            $emails = str_replace(array( '[','"',']' ), '', $emails);//replace brackets

            $MailData            = array();

            $MailData['from']    = $from;//any gmail id here

            $MailData['to'] = $emails;//'suvrodatta95@gmail.com';//authorized gmail only for free cause

            //$MailData['recipient-variables']      = '{"sample1@gmail.com": {"first":"Bob", "id":1}, "sample2@gmail.com": {"first":"Alice", "id": 2}}';
            $MailData['subject'] = $request->emailSubject;
            $MailData['text']    = $request->emailBody;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$api_key);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/'.$domain.'/messages');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $MailData);
            $result = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($result,true);

            return response()->json([

                'status'    => 'success',
                'message'   => 'Email sent!',
                'result'    => $response

            ],200);

        }catch(Throwable $e) {

            return response()->json([

                'status'    => 'error',
                'message'   => 'Error to fail!',
                'errors'    => $e->getMessage()

            ],423);

        }

    }
}
