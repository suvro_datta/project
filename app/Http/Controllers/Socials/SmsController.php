<?php

namespace App\Http\Controllers\Socials;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Throwable;
use App\Model\Customer;
use App\Model\SocialSettings;
use Illuminate\Validation\ValidationException;

class SmsController extends Controller
{

    function sendSms(Request $request){

        $errors = [];

        if(empty($request->twillioSid)){
            $errors['sid'][] = 'Twilio sid not found';
        }

        if(empty($request->twillioNumber)){
            $errors['number'][] = 'Twilio number not found';
        }

        if(empty($request->twillioAuthToken)){
            $errors['token'][] = 'Twilio auth token not found';
        }

        if( empty($request->twillioSid) || empty($request->twillioNumber) || empty($request->twillioAuthToken) ){
            return response()->json([
                'message'       => 'error',
                'errors'        => $errors
            ],423);
        }

        try{

            $request->validate([
                'message'=>'required',
            ]);

            $twillioSid         = $request->twillioSid;
            $from               = $request->twillioNumber;
            $twillioAuthToken   = $request->twillioAuthToken;
            $phoneNumbers       = $request->phoneNumbers;
            $messageBody        = $request->message;

            $total_number = 0;
            $total_sent   = 0;
            
            $apiUrl = "https://api.twilio.com/2010-04-01/Accounts/$twillioSid/Messages.json";

            foreach( $phoneNumbers as $phoneNumber ){
                $total_number++;
                try {

                    $id = $twillioSid;
                    $token = $twillioAuthToken;
                    $url = $apiUrl;

                    $data = array (
                        'From' => $from,
                        'To' => $phoneNumber,
                        'Body' => $messageBody,
                    );
                    $post = http_build_query($data);
                    $x = curl_init($url );
                    curl_setopt($x, CURLOPT_POST, true);
                    curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
                    curl_setopt($x, CURLOPT_POSTFIELDS, $post);
                    $y = curl_exec($x);
                    

                    $response = json_decode($y,true);
                    $status   = $response['status'];

                    if($status === 'queued'){
                        $total_sent++;
                    }

                    curl_close($x);

                    // var_dump($post);
                    // var_dump($y);

                    // Your Account SID and Auth Token from twilio.com/console
                    // $sid    = $twillioSid;//'AC1cb2e533e15da873b69f49f715d86a60';
                    // $token  = $twillioAuthToken;//'b1e1fbff848141b6cd5fdff02f665ea5';
                    // $client = new Client($sid, $token);

                    // // Use the client to do fun stuff like send text messages!
                    // $message = $client->messages->create(
                    //     // the number you'd like to send the message to
                    //     $phoneNumber,
                    //     [
                    //         // A Twilio phone number you purchased at twilio.com/console
                    //         'from' => $from,//+12513579700
                    //         // the body of the text message you'd like to send
                    //         'body' => $messageBody
                    //     ]
                    // );
                    
                    // $data = array(
                    //     'message'  => 'success',
                    //     'current'   => $phoneNumber
                    // );
                    
                    //echo (json_encode($data));

                }catch (Throwable $e) {
                    $error =  $e->getMessage();
                    // $data = array(
                    //     'message'  => 'error',
                    //     'current'   => $phoneNumber
                    // );
                    //echo (json_encode($data));
                }

            }

            return response()->json([
                'message'       => 'success',
                'totalNumber'   => $total_number,
                'totalSent'     => $total_sent,
            ]);

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }

    function getNumbersWithSmsSettings(){
        
        $customersPhone   = Customer::select('phone')->where('customer_type','regular_customer')->get();
        $smsSettings      = SocialSettings::select('sms_settings')->first();;

        $arr = json_decode($customersPhone, true);

        $phoneLists = array_column($arr, 'phone');
        
        return response()->json([

            'message'       => 'customers phone numbers and sms found successfully',
            'customers'     => $customersPhone,
            'smsSettings'   => $smsSettings,
            'phoneLists'    => $phoneLists

        ],200);
    }
}
