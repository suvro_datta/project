<?php

namespace App\Http\Controllers\Socials;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\SocialSettings;

class SocialController extends Controller
{
    function getSocialSettings(){

        $socialSettings = SocialSettings::first();

        return response()->json([
            'message'           => 'Social settings found!',
            'socialSettings'    =>  $socialSettings,
        ],200);

    }

    function saveSocialSettings(Request $request){

        $oldSettings = SocialSettings::first();

        if(empty($oldSettings)){
            //if not exists
            SocialSettings::create([

                'messenger_id'          =>  $request->messengerId,
                'whatsapp_phone_number' =>  $request->whatsappPhoneNumber,
                'sms_settings'          =>  $request->smsSettings,
                'email_settings'        =>  $request->emailSettings,
                
            ]);
        }else{
            //if exists
            $settingsId = $oldSettings->id;
            SocialSettings::find($settingsId)->update([

                'messenger_id'          =>  $request->messengerId,
                'whatsapp_phone_number' =>  $request->whatsappPhoneNumber,
                'sms_settings'          =>  $request->smsSettings,
                'email_settings'        =>  $request->emailSettings,

            ]);
        }

        return response()->json([
            'message'           => 'Social settings saved!',
            'socialSettings'    =>  $request->all(),
        ],200);

    }
}
