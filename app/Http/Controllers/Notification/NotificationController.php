<?php

namespace App\Http\Controllers\Notification;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    function markAsRead(){

        $data = auth()->user()->unreadNotifications->markAsRead();
        
        return response()->json([

            'message'    => 'All the notifications are marked as read!'

        ],200);

    }

    function getNotification(){

        return response()->json([

            'read_notifications'    => auth()->user()->readNotifications()->limit(5)->get(),
            'unread_notifications'  => auth()->user()->unreadNotifications,

        ],200);

    }
}
