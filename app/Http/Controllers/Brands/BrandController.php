<?php

namespace App\Http\Controllers\Brands;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use App\Model\Brand;

class BrandController extends Controller
{
    // public function all_brand(){
    //     return Brand::all();
    // }
    
    public function index(){

        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search   = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $Query   = Brand::select('*');

        if (!empty($search)) {

            $Query = $Query->where('name', 'LIKE', "%{$search}%");
            $Query->orWhere('id', 'LIKE', "%{$search}%");
          
        }

        $total            = $Query->count();
        $all_brands   = $Query->orderBy('id', 'desc')->paginate($paginate);

        return response()->json(['message' => 'Brand Created!','brands'=>$all_brands,
            'total' => $total,'search'=>$search]);

    }

    public function create(Request $request){

        try{

            $request->validate([
                'name'=>'required',
            ]);

            Brand::create(['name'=>$request->name]);

            return response()->json([
                'message'   => 'Brand created',
                'data'      => $request->all(),
                'status'    => 'success'
            ],201);

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }
       
    }

    public function update(Request $request,$id){
       
        try{

            $request->validate([
                'name'=>'required',
            ]);

            Brand::find($id)->update(['name'=>$request->name]);

            return response()->json([
                'message'   => 'Brand created',
                'data'      => $request->all(),
                'status'    => 'success'
            ],201);

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }

    public function edit($id){
        
        $brand = Brand::findOrFail($id);

        return response()->json([

            'status' => 'success',
            'msg'    => 'Edit data found',
            'editData' => $brand

        ], 201);

        // return response()->json([
        //     'status' => 'error',
        //     'msg'    => 'Error',
        //     'errors' => $exception->errors(),
        // ], 422);

    }

    public function delete($id){
        return Brand::destroy($id);
    }

    public function deletemultiple(Request $request){

        if( $request->has('rows') ){

            Brand::destroy(collect($request->rows)->pluck('id')->toArray());

            return response()->json([

                'status'    => 'success',
                'message'   => 'Data deleted!',
                'data'      => $request->rows
    
            ],201);

        }
        
    }
}
