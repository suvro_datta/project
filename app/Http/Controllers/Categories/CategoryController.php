<?php

namespace App\Http\Controllers\Categories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use App\Model\Category;

class CategoryController extends Controller
{
    // public function all_category(){
    //     return Category::all();
    // }

    public function index(){

        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search   = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $Query   = Category::select('*');

        if (!empty($search)) {

            $Query = $Query->where('name', 'LIKE', "%{$search}%");
            $Query->orWhere('id', 'LIKE', "%{$search}%");
          
        }

        $total            = $Query->count();
        $all_categories   = $Query->orderBy('id', 'desc')->paginate($paginate);

        return response()->json(['message' => 'Category Created!','categories'=>$all_categories,
            'total' => $total,'search'=>$search]);

    }

    public function create(Request $request){

        try{

            $request->validate([
                'name'=>'required',
            ]);

            Category::create(['name'=>$request->name]);

            return response()->json([
                'message'   => 'Category created',
                'data'      => $request->all(),
                'status'    => 'success'
            ],201);

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }
       
    }

    public function update(Request $request,$id){
       
        try{

            $request->validate([
                'name'=>'required',
            ]);

            Category::find($id)->update(['name'=>$request->name]);

            return response()->json([
                'message'   => 'Category created',
                'data'      => $request->all(),
                'status'    => 'success'
            ],201);

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }

    public function edit($id){
        
        $category = Category::findOrFail($id);

        return response()->json([

            'status' => 'success',
            'msg'    => 'Edit data found',
            'editData' => $category

        ], 201);

        // return response()->json([
        //     'status' => 'error',
        //     'msg'    => 'Error',
        //     'errors' => $exception->errors(),
        // ], 422);

    }

    public function delete($id){
        return Category::destroy($id);
    }

    public function deletemultiple(Request $request){

        if( $request->has('rows') ){

            Category::destroy(collect($request->rows)->pluck('id')->toArray());

            return response()->json([

                'status'    => 'success',
                'message'   => 'Data deleted!',
                'data'      => $request->rows
    
            ],201);

        }
        
    }
}
