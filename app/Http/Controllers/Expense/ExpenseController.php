<?php

namespace App\Http\Controllers\Expense;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

use App\Model\Expense;

class ExpenseController extends Controller
{

    public function index(){

        $paginate = isset($_REQUEST['paginate'])?$_REQUEST['paginate']:10;
        $search   = isset($_REQUEST['s'])?$_REQUEST['s']:'';

        $Query   = Expense::select('*');

        if (!empty($search)) {

            $Query = $Query->where('reference', 'LIKE', "%{$search}%");
            $Query->orWhere('description', 'LIKE', "%{$search}%");
            $Query->orWhere('id', 'LIKE', "%{$search}%");
            $Query->orWhere('total_expense', 'LIKE', "%{$search}%");
          
        }

        $total            = $Query->count();
        $all_expenses     = $Query->orderBy('id', 'desc')->paginate($paginate);

        return response()->json(['message' => 'Expense Created!','expenses'=>$all_expenses,
            'total' => $total,'search'=>$search]);

    }

    public function addExpense(Request $request){

        try{

            $request->validate([

                'reference'     =>  'required',
                'expense_date'  =>  'required',
                'description'   =>  'required',
                'total_expense' =>  'required|numeric',

            ]);

            $expense    = Expense::create([

                'reference'     =>  $request->reference,
                'expense_date'  =>  $request->expense_date,
                'description'   =>  $request->description,
                'total_expense' =>  $request->total_expense

            ]);

            return response()->json([

                'message'   => 'Expense Created!',
                'request'   =>  $request->all(),
                'expense'   =>  $expense

            ]);

        }catch (ValidationException $exception) {

            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);

        }

    }

    public function edit($id){

        $expense = Expense::findOrFail($id);

        return response()->json([

            'status' => 'success',
            'msg'    => 'Edit data found',
            'editData' => $expense

        ], 201);

    }

    public function update(Request $request,$id){

        try{

            $request->validate([

                'reference'     =>  'required',
                'expense_date'  =>  'required',
                'description'   =>  'required',
                'total_expense' =>  'required|numeric',

            ]);

            $expense = Expense::find($id)->update([

                'reference'     => $request->reference,
                'expense_date'  => $request->expense_date,
                'description'   => $request->description,
                'total_expense' => $request->total_expense,

            ]);

            return response()->json([

                'message'       => 'Expense updated!',
                'expense_data'  => $expense,
                'status'        => 'success'

            ],201);

        }catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }

    }

    public function delete($id){

        return Expense::destroy($id);

    }

    public function deletemultiple(Request $request){

        if( $request->has('rows') ){

            Expense::destroy(collect($request->rows)->pluck('id')->toArray());

            return response()->json([

                'status'    => 'success',
                'message'   => 'Data deleted!',
                'data'      => $request->rows
    
            ],201);

        }
        
    }

}
