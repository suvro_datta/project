<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Expense;
use App\Model\Order;

use Carbon\Carbon;
use DB;

class ReportController extends Controller
{
    public function getReports(Request $request){

        $fromDate   = $request->from;
        $toDate     = $request->to;

        $from   = date('Y-m-d', strtotime(substr($fromDate,0,10)));
        $to     = date('Y-m-d', strtotime(substr($toDate,0,10)));

        $rangeExpense = Expense::whereBetween('created_at', [$from.' 00:00:00',$to. ' 23:59:59'])
                        ->sum('total_expense');
        
        $rangeSell    = Order::whereBetween('created_at', [$from.' 00:00:00', $to. ' 23:59:59'])
                        ->sum('paid');

        // $fromToExpense = Expense::whereBetween('created_at', [$from.' 00:00:00',$to. ' 23:59:59'])
        //                 ->orderBy('created_at') 
        //                 ->groupBy('created_at')
        //                 ->get();
                         //->get();//->sum('total_expense');
        $fromToOrders   = DB::table('orders')
                        ->select(
                            DB::raw('YEAR(created_at) as year'),
                            DB::raw('MONTH(created_at) as month'),
                            DB::raw('DAY(created_at) as day'),
                            DB::raw('SUM(paid) as sum')
                        )
                        ->whereYear('created_at', '=', Carbon::now()->year)
                        ->orWhereYear('created_at', '=', Carbon::now()->subYear()->year)
                        ->groupBy('year', 'month','day')
                        ->get();

        $fromToExpenses = DB::table('expenses')
                        ->select(
                            DB::raw('YEAR(created_at) as year'),
                            DB::raw('MONTH(created_at) as month'),
                            DB::raw('DAY(created_at) as day'),
                            DB::raw('SUM(total_expense) as sum')
                        )
                        ->whereYear('created_at', '=', Carbon::now()->year)
                        ->orWhereYear('created_at', '=', Carbon::now()->subYear()->year)
                        ->groupBy('year', 'month','day')
                        ->get(); 

        return response()->json([

            'message'       => 'Reports found',
            'from'  => $from,
            'to'    => $to,
            'range_expense'     => $rangeExpense,
            'range_sell'        => $rangeSell,
            'fromToOrders'      => $fromToOrders,
            'fromToExpenses'    => $fromToExpenses,
            'request'   => $request->all()

        ]);

    }
}
