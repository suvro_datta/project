<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class ProfileController extends Controller
{
    public function getProfileInfo(){

        return response()->json([

            'message'   => 'Settings Not Found!',
            'userInfo'  => Auth::user()

        ]);

    }

    public function updateProfileInfo(Request $request){

        try{

            $request->validate([

                'name'    => 'required',
                'email'   => 'required',

            ]);

            $userId   =   Auth::user()->id;
            
            $user = User::find($userId)->update([

                'name'  => $request->name,
                'email' => $request->email,
                'phone' => $request->phone

            ]);

            return response()->json([

                'status' => 'success',
                'msg'    => 'Profile updated!',
                'user' => $user,

            ], 201);

        }catch (ValidationException $exception) {

            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);

        }

    }

    public function uploadImage(Request $request){

        $image = $request->file('image');

        //full image name with extension
        $image_name_with_ext = $image->getClientOriginalName();

		//image name without extension
        $image_name_without_ext = pathinfo($image_name_with_ext,PATHINFO_FILENAME);

        //get extension of that image
        $image_ext = $image->getClientOriginalExtension();

        //image name with encription
        $image_name_to_store = $image_name_without_ext.'_'.time().'_.'.$image_ext;

        //public folder
        $path = public_path('/images/profile');
        $image->move($path,$image_name_to_store);

        $imageName  =   $image_name_to_store;

        $userId     =   auth()->user()->id;
        
        $user = User::find($userId)->update([

            'image' => $imageName

        ]);

        return response()->json([

            'userId'      =>  $user,
            // 'message'   =>  $imageName

        ],201);

    }

    public function updatePassword(Request $request){

        $authUser = $request->authUser;
        $userId   = $authUser['user']['id'];

        $user = auth()->user();
        $value = $request->oldPassword;

        try{

            $request->validate([

                'oldPassword'   =>  ['required', function ($attribute, $value, $fail) use ($user) {
                    if (!\Hash::check($value, $user->password)) {
                        return $fail(__('The old password does not match.'));
                    }
                }],
                'password'      =>  'required|confirmed|min:6',

            ]);

            $user = User::find($userId)->update([
                'password' => Hash::make($request->password),
            ]);

            return response()->json([

                'message'   => 'Profile Settings Update',
                'authUser'  => $userId,
                'request'   => $request->password

            ],201);

        }catch (ValidationException $exception) {

            return response()->json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => $exception->errors(),
            ], 422);

        }

    }
}
