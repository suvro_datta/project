<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\AdvanceSettings;

class AdvanceSettingsController extends Controller
{
    public function index(){

        $settings   = AdvanceSettings::where('name','settings')->first();

        if(empty($settings)){
            
            $settings   = array(
                'settings'  => array(

                    'advance_settings'  =>  array(

                        'stripe_settings'   =>  array(

                            'publication_key'   => '',
                            'secret_key'        => ''
        
                        )

                    )

                )
            );

            

        }

        return response()->json(

            $settings,
   
        );

    }

    public function update(Request $request){
       

        $request_settings   = array(

            'advance_settings'  => array(

                'stripe_settings'   =>  array(

                    'publication_key'   => $request->stripePublicableKey,
                    'secret_key'        => $request->stripeSecretKey

                ),

            )

        );

        $db_settings   = AdvanceSettings::where('name','settings')->first();

        if(empty($db_settings)){

            $new_settings   =   AdvanceSettings::create([
                'name'      => 'settings',
                'settings'  => $request_settings,
            ]);

        }else{

            $id = $db_settings->id;
            AdvanceSettings::find($id)->update([
                'name'      => 'settings',
                'settings'  => $request_settings
            ]);

        }

        return response()->json([

            'message'   => 'Settings update',

        ]);
        
    }

}
