<?php 

use App\Model\Settings;

function settings(){

    $settings   = Settings::where('name','general_settings')->first();

    if(empty($settings)){
        
        $settings   = array(
            'settings'  => array(
                'general_settings'=>array(
                    'tax_settings'=>array(
                        'tax_type'   => 'flat',
                        'tax_rate'   => 0
                    ),
                    'currency_settings'  => array(
                        'currency_symbol'    => '$'
                    ),
                    'software_settings' => array(
                        'software_name' => 'fluentPos'
                    )
                )
            )
        );

        return $settings;

    }

    return $settings;
}