<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdvanceSettings extends Model
{
    
    protected $table = 'advance_settings';
    protected $casts    = ['settings' => 'array'];
    protected $fillable = [ 'name','settings' ];

}
