<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table    = 'expenses';
    protected $fillable = ['reference','expense_date','description','total_expense'];
}
