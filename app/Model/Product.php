<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="products";

    protected $fillable = [ 'name','images','price','description','featured','quantity','category_id','brand_id','barcode' ];

    public function category(){
    	return $this->belongsTo(Category::class,'category_id');
    }

    public function brand(){
    	return $this->belongsTo(Brand::class,'brand_id');
    }
}
