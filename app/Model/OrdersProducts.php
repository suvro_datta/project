<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrdersProducts extends Model
{

    protected $table = 'orders_products';
    protected $fillable = ['order_id','product_id','quantity'];

}
