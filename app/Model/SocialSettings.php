<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SocialSettings extends Model
{
    protected $table="social_settings";
    protected $casts    = ['sms_settings' => 'array','email_settings' => 'array'];
    protected $fillable = ['messenger_id','whatsapp_phone_number','sms_settings','email_settings'];
}
