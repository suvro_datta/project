<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = [ 'customer_type','phone','email','name','description','home_address','city','country' ];

    public function orders(){
    	return $this->hasMany(Order::class);
    }
    
}
